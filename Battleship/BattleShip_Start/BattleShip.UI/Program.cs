﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.UI.Workflow;

namespace BattleShip.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            //instantiate a new instance of a workflow class (folder) that will itself have two instantiated board objects
            //and then the rest of the functionality in a do...while structure will be in the workflow class
            //while the game is being played

            StartMenu start = new StartMenu();
            start.StoreNames();
            BattleshipWorkflow workflow = new BattleshipWorkflow();
            workflow.execute();
        }

    //How can the boards be set up?
    //The board class contains a ship object array that should only take in 5 ships
    //use the SetupBoard() class in the workflow

        //Instantiate a new 
        /*  var board = SetupBoard();  - This object contains 

            var coordinate = new Coordinate(15, 20);

            var response = board.FireShot(coordinate);

            Assert.AreEqual(ShotStatus.Invalid, response.ShotStatus);*/
    }
}
