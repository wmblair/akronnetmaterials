﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    class StartMenu
    {
        public string[] playerNames;

        public StartMenu()
        {
             playerNames = new string[2];
        }

        public void StoreNames()
        {
            Console.WriteLine("Welcome to Battleship!");

            Console.WriteLine("Please enter player1's name: ");
            playerNames[0] = Console.ReadLine();
            Console.WriteLine("Please enter player2's name: ");
            playerNames[1] = Console.ReadLine();
        }
    }
}
