﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;

namespace BattleShip.UI.Workflow
{
    class BattleshipWorkflow
    {
        public Board[] pBoard { get; set; }

        public BattleshipWorkflow()
        {
            pBoard = new Board[] {new Board(), new Board()};
        }

        public void execute()
        {
            string [,] battleshipGraph = new string[20,20];

            int whoPlay;
            for (whoPlay = 0; whoPlay < 2; whoPlay++)
            {
                Console.WriteLine("Player " + (whoPlay+1) + ": \n");
                PlaceShipRequest pRequest = new PlaceShipRequest();
                int shipCount = 0;
                do
                {

                    // string nameShip = (string) Enum.Parse(typeof(ShipDirection), i.ToString());

                    string nameShip = Enum.GetName(typeof(ShipType), shipCount);
                    Console.Write("Please enter a coordinate for " + nameShip + ": ");
                    string coord = Console.ReadLine();
                    pRequest.Coordinate = convertStringToCoordinate(coord);
                    Console.Write("Enter a direction for " + nameShip + " (0=Up, 1=down, 2=left, 3=right): ");
                    string dir = Console.ReadLine();
                    pRequest.Direction = (ShipDirection)Enum.Parse(typeof(ShipDirection), dir);

                    ShipPlacement sp = new ShipPlacement();
                    sp = pBoard[whoPlay].PlaceShip(pRequest);

                    Console.WriteLine(sp.ToString());

                    pRequest.ShipType = (ShipType)Enum.Parse(typeof(ShipType), shipCount.ToString()); //Enum.Parse results in an object
                    if (sp == ShipPlacement.Ok)
                        shipCount++;
                } while (shipCount < 5);
            }
            whoPlay = 0;

            do
            {
                setupGraph(pBoard[whoPlay].ShotHistory);
                
                Console.WriteLine("Player" +(whoPlay+1) + "'s turn");
                FireShotResponse answer = PromptPlayer(whoPlay);
                ShotAnswer(answer);
                Console.ReadLine();

                if ((int) Enum.Parse(typeof(ShotStatus), answer.ShotStatus.ToString()) > 1)
                    whoPlay = (whoPlay + 1) % 2;
                else if ((int) Enum.Parse(typeof(ShotStatus), answer.ShotStatus.ToString()) == 5)
                    break;
            } while (true);
        }

        public void ShotAnswer(FireShotResponse shotHistory)
        {
            string answer = shotHistory.ShotStatus.ToString();
            switch ((int)shotHistory.ShotStatus)
            {
                case 0:
                case 1:
                Console.WriteLine(answer + " - Try Again");
                //Console.WriteLine("Invalid coordinates.  Please try again: ");
                    break;
                case 2:
                Console.WriteLine(answer + " You hit something! - Next player's turn");
                    break;
                case 3:
                    Console.WriteLine(answer+" You sank your opponent's" + shotHistory.ShipImpacted.ToString() + " - next player's turn");
                    break;
                case 4:
                    Console.WriteLine(answer + " Your projectile splashes into the ocean...you missed! - next player's turn");
                    break;
                default:
                    Console.WriteLine(answer+" You have sunk all your opponent's ships, you win! - end game");
                    break;
                    
            }
        }

        public FireShotResponse PromptPlayer(int whichPlayer)
        {
            Console.WriteLine("What coordinates do you want to fire at?");
            string shot = Console.ReadLine();
            
            FireShotResponse fireShot = new FireShotResponse();
            fireShot = pBoard[whichPlayer].FireShot(convertStringToCoordinate(shot));
          // Console.WriteLine(fireShot.ShotStatus.ToString());
            return fireShot;
        }

        public void setupGraph(Dictionary<Coordinate, ShotHistory> dictionaryHistory)
        {
            for (int i = 0; i < 21; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Coordinate newLocation = new Coordinate((i+1),(j+1));
                    if (i == 20 && j == 0)
                        Console.Write((i/2)+" ");
                    else if (j == 0 && i%2 == 0)
                        Console.Write(" "+(i/2)+" ");
                    else if (j == 0 && i%2 == 1)
                        Console.Write("----");
                    if (i == 0)
                    {
                        Console.Write("| " + Convert.ToChar(j+65) + " ");
                    }
                    else if (i%2 == 1)
                        Console.Write("----");
                    else if (dictionaryHistory.ContainsKey(newLocation))
                        Console.Write("| M ");
                    else
                        Console.Write("|   ");
                }
                Console.WriteLine("");
            }
            
        }

        public void updateGraph(/*string [,] drawnBoard*/)
        {
            for (int i = 0; i < 20; i++)
            {
                Console.WriteLine("------------------------------------------------------------");
                for (int j = 0; j < 20; j++)
                {
                   // Console.WriteLine(drawnBoard[i,j]);
                   Console.Write(" | ");

                }
            }
        }

        public Coordinate convertStringToCoordinate(string s)
        {
            Coordinate c = new Coordinate(TranslateCoordinates(s[0]), int.Parse(s.Substring(1)));
            return c;
        }

        public int TranslateCoordinates(char coordinate)
        {
            int index = char.ToUpper(coordinate) - 64;
            return index;
        }
    }
}
